// 云函数入口文件
const cloud = require('wx-server-sdk')

const request = require('request');

cloud.init()
const db = cloud.database();

// 云函数入口函数
exports.main = async(event, context) => {
  let error = '';
  let access_tokenStr = event.access_token || '';
  let pictureUrl = 'https://aip.baidubce.com/rest/2.0/ocr/v1/general?access_token=';
  // 请求百度数据
  try {
    if (!event.access_token) {
      const resultBaiDuGetA = await new Promise((resolve, reject) => {
        request.get('https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=dvUo1F4fXQQTxfmxmRaB9OD7&client_secret=gbURktgNti24phtCmEdgMcG3oyHqLO1K', function(error, response, body) {
          return error ? resolve(error) : resolve(body);
        })
      })
      // resultBaiDuGetA = JSON.parse(resultBaiDuGetA);
      if (JSON.parse(resultBaiDuGetA).access_token) {
        access_tokenStr = JSON.parse(resultBaiDuGetA).access_token;
      } else {
        error += '调用百度密钥出现问题！';
        return {
          'err': error,
          'ss': JSON.parse(resultBaiDuGetA)
        };
      };
    };
  } catch (err) {
    return '请求百度密钥出现错误！'
  }

  const fileList = event.fileID
  // 添加到数据库
  try {
    let resDatabase = await db.collection("pictureTextImage").add({
      data: {
        description: "",
        fileID: fileList[0],
        creatData: new Date()
      }
    })
    // return resDatabase;
  } catch (err) {
    return "添加数据集合出现问题！"
  };
  try {
    const result = await cloud.getTempFileURL({
      fileList,
    })
    if (result.fileList && result.fileList.length > 0 && result.fileList[0].tempFileURL) {
      const resGetText = new Promise((resolve, reject) => {
        request.post(pictureUrl + access_tokenStr, {
          form: {
            url: result.fileList[0].tempFileURL,
            detect_direction: true
          }
        }, function(error, response, body) {
          return error ? resolve(error) : resolve({
            'body': JSON.parse(body),
            'access_tokenStr': access_tokenStr
          });
        })
      })
      return resGetText;
    }
  } catch (err) {
    return "图片解析出现问题！";
  }
  // return result.fileList[0].tempFileURL




}