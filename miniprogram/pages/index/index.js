//index.js
const app = getApp()

Page({
  data: {
    avatarUrl: './user-unlogin.png',
    userInfo: {},
    dataLoad: false,
    logged: false,
    takeSession: false,
    requestResult: '',
    imgSrc: '',
    access_token: '',
    phones:[],
    responseDataStr: ''
  },

  onLoad: function () {
    wx.showLoading({
      title: '加载中……',
    });
    // 获取
    // if (!wx.cloud) {
    //   wx.redirectTo({
    //     url: '../chooseLib/chooseLib',
    //   })
    //   return
    // }

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo,
                logged: true
              })
            },
            complete: () => {
              wx.hideLoading();
            }
          })
        }else{
          wx.hideLoading();
        };
      }
    })
    // 设置转发按钮
    wx.showShareMenu({
      withShareTicket: true
    })
  },
  onReady: function(){
    this.setData({
      dataLoad:true
    })
  },
  onGetUserInfo: function(e) {
    if (!this.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
  },
  // 随机生成唯一码
  createRandomId: function() {
    return (Math.random() * 10000000).toString(16).substr(0, 4) + '_' + (new Date()).getTime() + '_' + Math.random().toString().substr(2, 5);
  },
  // 上传图片
  doUpload: function() {
    const that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {

        wx.showLoading({
          title: '上传中',
        })
        const filePath = res.tempFiles[0].path;
        // 上传图片
        const cloudPath = that.createRandomId() + filePath.match(/\.[^.]+?$/)[0]
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: res => {
            // console.log('[上传文件] 成功：', res)
            wx.showLoading({
              title: '图片解析中',
            })

            app.globalData.fileID = res.fileID
            app.globalData.cloudPath = cloudPath
            app.globalData.imagePath = filePath
            // 执行图片文字解析函数
            that.pictureText(res.fileID);
            that.setData({
              imgSrc: filePath
            })
            // 跳转路径
            // wx.navigateTo({
            //   url: '../storageConsole/storageConsole'
            // })
          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          }
        })

      },
      fail: e => {
        console.error(e)
      }
    })
  },
  // 解析图片上的文字
  pictureText: function(fileId) {
    const that = this;
    wx.cloud.callFunction({
      name: 'pictureText',
      data: {
        access_token: this.access_token,
        fileID: [fileId]
      },
      success(res) {
        // console.log(res.result)
        if (res.result.access_tokenStr) {
          that.setData({
            access_token: res.result.access_tokenStr
          })
        };
        if (res.result && res.result.body) {
          that.setData({
            responseDataStr: res.result.body.words_result.map(function(item) {
              return item.words;
            }),
            phones: JSON.stringify(res.result.body.words_result).match(/0?(13|14|15|17|18|19)[0-9]{9}/g)
          })
          // console.log(that.data)
        }
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },
  phoneCall: function(data){
    wx.makePhoneCall({
      phoneNumber: data.target.dataset.no
    })
  }
})